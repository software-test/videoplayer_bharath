#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QMessageBox>
#include <QDebug>
#include <QFile>
#include <QDateTime>
#include<QThread>
using namespace std;

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
     ui->setupUi(this);

     myPlayer = new Player();
     QObject::connect(myPlayer, SIGNAL(processedImage(QImage)),
                              this, SLOT(updatePlayerUI(QImage)));


     timer=new QTimer(this);
     connect(timer,SIGNAL(timeout()),this,SLOT(myfunctiontime()));
     timer->start(1000);

     QDate currentdate=QDate::currentDate();
     QString date_text=currentdate.toString("DATE : dd/MM/yy");
     ui->date_label_2->setText(date_text);




    //Shortcut F11
    keyF11 = new QShortcut(this);   // Initialize the object
    keyF11->setKey(Qt::Key_F11);    // Set the key code
    // connect handler to keypress
    //  connect(keyF11, SIGNAL(activated()), this, SLOT(slotShortcutF11()));


    //Shortcut F2
    keyF2 = new QShortcut(this);   // Initialize the object
    keyF2->setKey(Qt::Key_F2);    // Set the key code
    // connect handler to keypress
    connect(keyF2, SIGNAL(activated()), this, SLOT(slotShortcutF2()));
}
MainWindow::~MainWindow()
{
    delete ui;
//    delete myPlayer;
}


void MainWindow::on_pushButton_5_clicked()
{
    ui->stackedWidget->setCurrentIndex(1);
    QMessageBox::about(this,"INFO","You Entered Video Settings");//Display Info Message
}

void MainWindow::on_pushButton_6_clicked()
{
    ui->stackedWidget->setCurrentIndex(2);
    QMessageBox::about(this,"INFO","You Entered Shortcut Menu");
}
void MainWindow::on_pushButton_7_clicked()
{
    ui->stackedWidget->setCurrentIndex(3);
    QMessageBox::about(this,"INFO","You Entered Patient Registration Form");
}
void MainWindow::on_pushButton_8_clicked()
{
    ui->stackedWidget->setCurrentIndex(0);
    QMessageBox::about(this,"INFO","You Entered Main Menu");
}


//void MainWindow::slotShortcutF11()
//{
//       x=vw->size();
//    qDebug()<<x;
//  vw->resize(350,350);
//  if(toggler==true)
//   {
//       qDebug()<<"full screen";
//      Qlabel(350,350);
//       toggler =true;
//   }
//   if( toggler ==true)
//   {
//     qDebug()<<"small scr";
//       vw->resize(590,490);
//     toggler =false;
//  }
//}

void MainWindow::slotShortcutF2()
{
    if (myPlayer->isStopped())
    {
        myPlayer->Play();

    }else
    {
        myPlayer->Stop();

    }
}

//void MainWindow::on_Brightness_valueChanged(int value)
//{
//      brightness = (float)value;
//}


void MainWindow::on_pushButton_3_clicked()
{
    QFile file1("/home/htic/Videoplayer/save.text");
    file1.open(QIODevice::WriteOnly | QIODevice::Text);//Create a document for the output file
    QApplication::processEvents();
    QString b,c,d,e,f;
    b=ui->lineEdit->text();
    c=ui->lineEdit_2->text();
    d=ui->lineEdit_3->text();
    e=ui->lineEdit_4->text();
    f=ui->lineEdit_5->text();

    QTextStream out(&file1);
    out<<"First Name:"<<b<<endl<<"Last Name : "<<c<<endl<<"Patient Id : "<<d<<endl<<"Gender : "<<e<<endl<<"Age : "<<f;

      QMessageBox::about(this,"INFO","Value Saved");

      ui->patient_name_label_2->setText("Patient's First Name :"+b);
      ui->patient_lname_label_2->setText("Patient's Last Name :"+c);
      ui->patient_id_label_2->setText("Patient ID :"+d);
      ui->patient_gender_label_2->setText("Gender :"+e);
      ui->patient_age_label_2->setText("Age :"+f);
 }



void MainWindow::updatePlayerUI(QImage img)
{

    if (!img.isNull())
    {

        ui->label_8->setAlignment(Qt::AlignCenter);
        ui->label_8->setPixmap(QPixmap::fromImage(img).scaled(ui->label_8->size(),
                                           Qt::KeepAspectRatio, Qt::FastTransformation));

//        if(keyF11)
//        {
//            qDebug()<<"f11";
//            int w = ui->label_8->width();
//            int h = ui->label_8->height();
//            w=w/2;
//            h=h/2;
//            qDebug()<<w;
//            qDebug()<<h;
//            ui->label_8->setAlignment(Qt::AlignCenter);
//            ui->label_8->setPixmap(QPixmap::fromImage(img).scaled(w,h,
//                                               Qt::KeepAspectRatio, Qt::FastTransformation));
//            QThread::msleep(50);
 //       }


    }
}

void MainWindow::on_pushButton_clicked()
{
    QString filename = QFileDialog::getOpenFileName(this,
                                          tr("Open Video"), ".",
                                          tr("Video Files (*.avi *.mpg *.mp4)"));
    if (!filename.isEmpty()){
        if (!myPlayer->loadVideo(filename.toLatin1().data()))
        {
            QMessageBox msgBox;
            msgBox.setText("The selected video could not be opened!");
            msgBox.exec();
        }
    }

    myPlayer->Play();
}



void MainWindow::myfunctiontime()
{
    QTime time=QTime::currentTime();
    QString time_text=time.toString("TIME : hh: mm: ss");
    ui->digital_clock_2->setText(time_text);
}


