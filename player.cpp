#include "player.h"
using namespace cv;
using namespace std;

Player::Player(QObject *parent)
 : QThread(parent)
{
    stop = true;
}
bool Player::loadVideo(string filename) {
    capture.open(filename);
    if (capture.isOpened())
    {
        frameRate = (int) capture.get(CAP_PROP_FPS);
        return true;
    }
    else
        return false;
}
void Player::Play()
{
    if (!isRunning()) {
        if (isStopped()){
            stop = false;
        }
        start(LowPriority);
    }
}
void Player::run()
{
 //  int brightness=1;
    int delay = (1000/frameRate);
    while(!stop){
        if (!capture.read(frame))
        {
            stop = true;
        }
        if (frame.channels()== 3){
            cv::cvtColor(frame, RGBframe, cv::COLOR_BGR2RGB);
            img = QImage((const unsigned char*)(RGBframe.data),
                              RGBframe.cols,RGBframe.rows,QImage::Format_RGB888);
        }
        else
        {
            img = QImage((const unsigned char*)(frame.data),
                                 frame.cols,frame.rows,QImage::Format_Indexed8);
        }
//        uchar *line =img.scanLine(0);
//        uchar *pixel = line;

//        for (int y = 0; y < img.height(); ++y)
//        {
//            pixel = line;
//            for (int x = 0; x < img.width(); ++x)
//            {
//                *pixel = qBound(0, *pixel + brightness, 255);
//                *(pixel + 1) = qBound(0, *(pixel + 1) + brightness, 255);
//                *(pixel + 2) = qBound(0, *(pixel + 2) + brightness, 255);
//                pixel += 4;
//            }

//            line += img.bytesPerLine();
 //       }
        emit processedImage(img);
        this->msleep(delay);
    }
}

void Player::Stop()
{
    stop = true;
}

void Player::msleep(int ms){
    struct timespec ts = { ms / 1000, (ms % 1000) * 1000 * 1000 };
    nanosleep(&ts, NULL);
}

bool Player::isStopped() const{
    return this->stop;
}

