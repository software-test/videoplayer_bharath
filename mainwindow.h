#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QMediaPlayer>
#include <QVideoWidget>
#include <QMediaPlaylist>
#include <QUrl>
#include <QShortcut>

#include <QFileDialog>
#include <QMessageBox>
#include <player.h>
#include <QTimer>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
//    void slotShortcutF11( );

    void slotShortcutF2();

    void on_pushButton_5_clicked();

    void on_pushButton_6_clicked();

    void on_pushButton_7_clicked();

    void on_pushButton_8_clicked();

//    void on_Brightness_valueChanged(int value);

    void on_pushButton_3_clicked();

//    void on_pushButton_2_clicked();

    void on_pushButton_clicked();

     void updatePlayerUI(QImage img);

     void myfunctiontime();


private:
   // void positionChanged(qint64);

    Ui::MainWindow *ui;
    QShortcut *keyF11;
    QShortcut *keyF2;
    int brightness;
    QSize x;
    Player* myPlayer;
    QTimer *timer;

    bool toggler=true;
};
#endif // MAINWINDOW_H
